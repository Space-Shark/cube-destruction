﻿using UnityEngine;
using System.Collections;

public class BricksController : MonoBehaviour {

	public GameObject brickParticle;

	public AudioClip breakAudio;

	private GameObject myBrickParticle;

	void Start() {
		
	}

	void DestroyRest() {
		Destroy (myBrickParticle);
		Destroy (gameObject);
	}

	void OnCollisionEnter(Collision other) {
		if (other.gameObject.CompareTag ("Ball")) {
			myBrickParticle = (GameObject)Instantiate (brickParticle, transform.position, Quaternion.identity);
			GameManager.instance.DestroyBrick ();
			AudioSource.PlayClipAtPoint (breakAudio, transform.position);
			gameObject.SetActive (false);
			Invoke ("DestroyRest", 1);
		}
	}
}
