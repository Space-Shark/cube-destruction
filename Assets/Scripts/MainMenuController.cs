﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	public Button playButton;
	public Button quitButton;

	void Start() {
		EventSystem.current.SetSelectedGameObject (playButton.gameObject);
	}

	public void Play() {
		SceneManager.LoadScene ("Level1");
	}

	public void Quit() {
		Application.Quit ();
	}
}
