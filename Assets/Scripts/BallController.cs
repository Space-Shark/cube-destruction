﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {

	public float ballInitialVelocity = 600f;

	private Rigidbody rb;
	private bool inPlay;

	public AudioClip bounceSFX;

	// Use this for initialization
	void Awake () {
		rb = gameObject.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1") && !inPlay) {
			transform.parent = null;
			inPlay = true;
			rb.isKinematic = false;
			rb.AddForce (new Vector3 (ballInitialVelocity, ballInitialVelocity));
		}
	}

	void OnCollisionEnter(Collision other) {
		if (other.gameObject.CompareTag ("Wall")) {
			AudioSource.PlayClipAtPoint (bounceSFX, transform.position);
		}
	}
}
