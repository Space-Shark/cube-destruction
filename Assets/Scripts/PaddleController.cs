﻿using UnityEngine;
using System.Collections;

public class PaddleController : MonoBehaviour {

	public float paddleSpeed;

	private Vector3 playerPosition;

	// Use this for initialization
	void Start () {
		Reset ();
	}
	
	// Update is called once per frame
	void Update () {
		float xPos = transform.position.x + Input.GetAxis ("Horizontal") * paddleSpeed;
		playerPosition = new Vector3 (Mathf.Clamp (xPos, -8f, 8f), playerPosition.y, playerPosition.z);
		transform.position = playerPosition;
	}

	public void Reset() {
		playerPosition = new Vector3 (0, -9.5f, 0);
	}
}
