﻿using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour {

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("Ball") && GameManager.instance.deathOn) {
			GameManager.instance.LoseLife ();
		}
	}
}
