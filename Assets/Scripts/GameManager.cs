﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public int numLives = 3;
	public int numBricks = 20;
	public int score = 0;
	public float resetDelay = 1f;
	public bool deathOn = true;

	public Text livesText;
	public Text scoreText;
	public Text gameOverText;
	public Text levelCompleteText;

	public GameObject bricksPrefab;
	public GameObject paddle;
	public GameObject deathParticles;

	public static GameManager instance = null;

	private GameObject clonePaddle;

	private List<GameObject> deathParticleList = new List<GameObject>();

	// Use this for initialization
	void Awake () {
		// Good habit, apparently, to create a singleton
		// Game Manager so you don't end up with mutliple
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		Setup ();
	}

	void Setup () {
		SetupPaddle ();
		Instantiate (bricksPrefab, transform.position, Quaternion.identity);
	}

	void SetupPaddle() {
		clonePaddle = Instantiate (paddle, transform.position, Quaternion.identity) as GameObject;
	}

	void CheckGameOver() {
		if (numBricks < 1) {
			levelCompleteText.gameObject.SetActive (true);
			Time.timeScale = .25f;
			deathOn = false;
			Invoke ("Reset", resetDelay);
		}

		if (numLives < 1) {
			gameOverText.gameObject.SetActive (true);
			Time.timeScale = 0.25f;
			deathOn = false;
			Invoke ("Reset", resetDelay);
		}
	}

	void Reset() {
		Time.timeScale = 1f;
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	public void LoseLife() {
		numLives--;
		livesText.text = "Lives: " + numLives;
		deathParticleList.Add((GameObject)Instantiate (deathParticles, clonePaddle.transform.position, Quaternion.identity));
		Invoke ("DestroyParticles", 1f);
		Destroy (clonePaddle);
		Invoke ("SetupPaddle", resetDelay);
		CheckGameOver ();
	}

	void DestroyParticles() {
		for (int i = 0; i < deathParticleList.Count; i++) {
			Destroy (deathParticleList [i]);
		}
		deathParticleList.Clear ();
	}

	public void DestroyBrick() {
		numBricks--;
		score++;
		scoreText.text = score + " :Score";
		CheckGameOver ();
	}
}
