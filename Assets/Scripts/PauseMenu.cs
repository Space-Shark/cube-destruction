﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;

public class PauseMenu : MonoBehaviour {

	public GameObject pauseMenu;

	public int mainMenuLevel;

	public Button firstSelected;

	private bool paused;

	void Start() {
		paused = false;
	}

	void Update() {
		if (Input.GetButtonDown ("Start")) {
			if (paused)
				ResumeGame ();
			else
				PauseGame ();
		}
	}

	public void PauseGame() {
		Time.timeScale = 0f;
		pauseMenu.SetActive (true);
		firstSelected.Select ();
	}

	public void ResumeGame() {
		Time.timeScale = 1f;
		pauseMenu.SetActive (false);
	}

	public void QuitToMainMenu() {
		Time.timeScale = 1f;
		pauseMenu.SetActive (false);
		SceneManager.LoadScene (mainMenuLevel);
	}
}
